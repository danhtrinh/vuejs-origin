import Vue from 'vue';
import Vuex from 'vuex';

import { counter } from './Counter/counter.module';
import { movie } from './Movie/movie.module'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        counter,
        movie
    }
});