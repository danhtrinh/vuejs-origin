import { movieApi } from '../../api/movieApi'

const state = {
    dataMovies: {}
}

const actions = {
    getMovie({commit}) {
        movieApi.getAll().then(response => {
            commit('getMovieRequest', response.data)
        })
    },
    clear({commit}) {
        commit('clearRequest')
    }
}

const mutations = {
    getMovieRequest(state, dataMovies) {
        state.dataMovies = dataMovies
    },
    clearRequest(state) {
        state.dataMovies = {}
    }
}

export const movie = {
    namespaced: true,
    state,
    actions,
    mutations
};
