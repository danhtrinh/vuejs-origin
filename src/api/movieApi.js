import axios from 'axios'

export class movieApi {
    static getAll() { 
        return axios({
            method: 'get',
            url: 'https://facebook.github.io/react-native/movies.json',
            headers: {
                'Content-Type': 'application/json'
            }   
        })
    } 
}